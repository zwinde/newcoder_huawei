class item:
    def __init__(self, c, v):
        self.cost = c;
        self.val = v;
        self.father = 0;

n, m =  map(int, input().split());

lst = [];
mm = [0 for i in range(m + 1)];
accessory = [];

for i in range(m):
    cost, value, father = map(int, input().split());
    if father == 0:
        group =[item(cost, value * cost)];
        mm[i + 1] = len(lst);
        lst.append(group);
    if father > 0:
        t = item(cost, value * cost);
        t.father = father;
        accessory.append(t);

for i in range(len(accessory)):
        group = lst[mm[accessory[i].father]];
        cost =  accessory[i].cost;
        val =  accessory[i].val;
        for j in range(len(group)):
            group.append(item(cost + group[j].cost, val + group[j].val));
        lst[mm[accessory[i].father]] = group;

dp = [0 for col in range(n + 1)];

for g in range(len(lst)):
    for c in range(n, -1, -1):
        for it in range(len(lst[g])):
            if (c >= lst[g][it].cost):
                dp[c] = max(dp[c], dp[c - lst[g][it].cost] +  lst[g][it].val);
print(dp[n]);
