import sys;

while True:
    str = sys.stdin.readline().strip()
    if str == "":
        break
    s = '';
    for c in str:
        if 'A'<= c <= 'Z':
            s +=chr( (ord(c) - ord('A') + 1)% 26 + ord('a'));
        elif c  in '0123456789':
            s +=c;
        elif c in 'abc':
            s += '2';
        elif c in 'def':
            s += '3';
        elif c in 'ghi':
            s += '4';
        elif c in 'jkl':
            s += '5';
        elif c in 'mno':
            s += '6';
        elif c in 'pqrs':
            s +='7';
        elif c in 'tuv':
            s += '8';
        elif c in 'wxyz':
            s += '9';
    print(s);
