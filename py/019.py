import sys;

class record:
    def __init__(self, name, line, cnt):
        self.name = name;
        self.line = line;
        self.cnt = cnt;

lst = [];
while True:
    str = sys.stdin.readline().strip()
    if str == "":
        break
    name = str.split()[0].split('\\')[-1];
    if len(name) > 16:
        name = name[-16:];
    line= int(str.split()[1]);
    #temp_sum = 0;
    found = False;
   
    for i in range(len(lst)):
        if lst[i].name == name and lst[i].line == line:
            lst[i].cnt +=1;
            found = True;
            #temp_sum = lst[i].cnt;
            #lst.pop(i);
            break;
    if not found:
        lst.append(record(name, line, 1));

if len(lst) <= 8:
    for i in range(len(lst)):
        print(lst[i].name, lst[i].line, lst[i].cnt, end='\n');
else:
    for i in range(-8, 0):
        print(lst[i].name, lst[i].line, lst[i].cnt, end='\n');