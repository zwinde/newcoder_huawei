import sys;
MaskList =[254, 252, 248, 240, 224, 192, 128, 0];
d = {'A': 0, 'B': 0, 'C': 0, 'D': 0, 'E': 0, 'I': 0, 'P': 0};

class IpAdd:
    def __init__(self, str):
        self.legal = True;
        self.a = [0]*4;
        for i in range(4):
            if str.split('.')[i] =='':
                self.legal = False;
            if self.legal:
                if 0<=int(str.split('.')[i])<=255:
                    self.a[i] = int(str.split('.')[i]);
                else:
                    self.legal = False;

def checkMask(ip, i, mustZero):
    if ip.a[0]==0 or ip.a[3] == 255:
        return False;
    if i==4: 
        return True;
    if not mustZero:
        if ip.a[i] == 255:
            return checkMask(ip, i+1, False);
        if ip.a[i] in MaskList:
            return checkMask(ip, i+1, True);
        return False;
    else:#mustZero
        if ip.a[i] == 0:
            return checkMask(ip, i+1, True);
        return False;

def inRange(ip, i, min, max):
    return min<= ip.a[i] <= max;


while True:
    str = sys.stdin.readline().strip()
    if str == "":
        break
    ip = IpAdd(str.split('~')[0]);
    mask = IpAdd(str.split('~')[1]);
    if not ip.legal or not mask.legal or not checkMask(mask, 0, False):
        d['I'] += 1;
        continue;
    if inRange(ip, 0, 1, 126):
        d['A'] += 1;
    if inRange(ip, 0, 128, 191):
        d['B'] += 1;
    if inRange(ip, 0, 192, 223):
        d['C'] += 1;
    if inRange(ip, 0, 224, 239):
        d['D'] += 1;
    if inRange(ip, 0, 240, 255):
        d['E'] += 1;
    if inRange(ip, 0, 10, 10):
        d['P'] += 1;
    if inRange(ip, 0, 172, 172) and inRange(ip, 1, 16, 31):
        d['P'] += 1;
    if inRange(ip, 0, 192, 192) and inRange(ip, 1, 168,168):
        d['P'] +=1;

print(d['A'],  d['B'],  d['C'],  d['D'],  d['E'],  d['I'],  d['P']);

