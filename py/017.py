while True:
    try:     
        x = 0;
        y = 0;
        lst = input().split(';');
        for s in lst:
            if len(s)<2:
                continue;
        
            if s[0] == 'A':
                flag = True;
                for chr in s[1:]:
                    if chr < '0' or chr > '9':
                        flag = False;
                        break;
                if flag==True:
                    x -= int(s[1:]);
        
            if s[0] == 'W':
                flag = True;
                for chr in s[1:]:
                    if chr < '0' or chr > '9':
                        flag = False;
                        break;
                if flag==True:
                    y += int(s[1:]);    
             
            if s[0] == 'D':
                flag = True;
                for chr in s[1:]:
                    if chr < '0' or chr > '9':
                        flag = False;
                        break;
                if flag==True:
                    x += int(s[1:]);
        
            if s[0] == 'S':
                flag = True;
                for chr in s[1:]:
                    if chr < '0' or chr > '9':
                        flag = False;
                        break;
                if flag==True:
                    y -= int(s[1:]);
        print (x, y, sep=',');
    except:
        break;
