import sys
while True:
    try:
        ll = list(input().split(' ')[1:]);
        rr = list(map(int, input().split()[1:]));

        rr.sort();
        i = 1;
        while i < len(rr):
            if rr[i] == rr[i-1]:
                rr.pop(i);
            else:
                i += 1;
       
        out, sum = "", 0;
        for i in rr:
            t_lst, cnt = "", 0;
            for j in range(len(ll)):
                if ll[j].find(str(i)) != -1:
                    cnt += 1;
                    t_lst += str(j) + ' ' + ll[j] + ' ';
            if cnt > 0 :
                out += str(i) + ' ' + str(cnt)+ ' ' + t_lst;
                sum += 2 + cnt * 2;
        print (sum, out);
    except:
        break
