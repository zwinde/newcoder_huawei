while True:
    try:
        str = input();
        lst = [0] * 129;
        for chr in str:
            if 0<= ord(chr) < 128:
                lst[ord(chr)] = 1;
        print(sum(lst));
    except:
        break;
