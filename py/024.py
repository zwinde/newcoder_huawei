import sys
while True:
    try:
        n = int(input());
        h = list(map(int, sys.stdin.readline().strip().split()));
        dp_r = [1] * n;
        dp_l = [1] * n;
        for i in range(1, n):
            m = 0;
            for j in range(0, i):
                if dp_l[j] > m and h[j]<h[i]:
                    m= dp_l[j];
            dp_l[i] = m + 1;

        for i in range(-2, -n-1, -1):
            m = 0;
            for j in range(-1, i, -1):
                if dp_r[j] > m and h[j] < h[i]:
                    m= dp_r[j];
            dp_r[i] = m + 1;

        m = 0;
        for i in range (n):
            if (dp_l[i] + dp_r[i]) > m:
                m = dp_l[i] + dp_r[i];

        print (n - m + 1);
    except:
        break