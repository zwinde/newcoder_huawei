import sys
while True:
    try:
        n = int(sys.stdin.readline().strip());
        if n==0:
            break;
        sum = 0;
        while n>=3:
            sum += n//3;
            n = n%3 + n//3;
        if n == 2:
            sum += 1;
        print(sum);
    except:
        break
