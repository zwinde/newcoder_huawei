import sys

while True:
    try:
        n = int(input());
        v = [0] * 10000;
        max =0;
        for i in range(n):
            ind, val =map(int,(input().split()))
            ind = int(ind);
            val = int(val);
            v[ind] += val;
            if ind> max:
                max = ind;
        for i in range(max + 1):
            if  v[i] != 0 :
                print (i, v[i]);
    except:
        break;