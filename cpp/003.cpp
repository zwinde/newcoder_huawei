#include <iostream>

using namespace std;

int main()
{
	int n, t;
	while (cin >> n)
	{
		bool exist[1005] = { 0 };
		for (int i = 0; i < n; i++)
		{
			cin >> t;
			exist[t] = true;
		}
		for (int i = 0; i <= 1000; i++)
		{
			if (exist[i])
				cout << i << endl;
		}
	};
	return 0;
}