#include <iostream>
#include <string>
using namespace std;

int main()
{
	string str;
	while (cin >> str)
	{
		int cnt = 0;
		while (cnt < str.length())
		{
			cout << str[cnt];
			cnt++;
			if (cnt % 8 == 0)
				cout << endl;
		}
		if (str.length() % 8 != 0)
		{
			 for (int i = 8; i > (str.length() % 8); i--)
			 {
				 cout << '0';
			 }
			 cout << endl;
		 }
	}
	return 0;
}