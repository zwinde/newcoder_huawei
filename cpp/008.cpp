#include <iostream>

using namespace std;
int main()
{
	int n = 0;
	while (cin >> n)
	{
		int v[1000] = { 0 };
		int max = 0;
		for (int i = 0; i < n; i++)
		{
			int ind, val;
			cin >> ind >> val;
			v[ind] += val;
			max = ind > max ? ind : max;
		}
		for (int i = 0; i <= max; i++)
		{
			if (v[i] != 0)
			{
				cout << i << ' ' << v[i] << endl;
			}
		}
	}
	return 0;
}