#include<iostream>
#include <string>
using namespace std;

int main()
{
	string str;
	while (cin >> str)
	{
		int dec = 0;
		int temp = 0;
		for (int i = 2; i < str.length(); i++)
		{
			temp = 0;
			if (str[i] >= 'A' && str[i] <= 'Z')
				temp = str[i] - 'A' + 10;
			if (str[i] >= 'a' && str[i] <= 'z')
				temp = str[i] - 'a' + 10;
			if (str[i] >= '0' && str[i] <= '9')
				temp = str[i] - '0';
			dec = dec * 16 + temp;
		}
		cout << dec <<endl;
	}
}